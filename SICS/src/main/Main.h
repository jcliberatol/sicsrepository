/*
 * SICS.h
 *
 *  Created on: May 27, 2014
 *      Author: mirt
 */

#ifndef SICS_H_
#define SICS_H_

#include <iostream>
#include <type/Matrix.h>
#include <boost/dynamic_bitset.hpp>
#include <type/PatternMatrix.h>
#include <model/Model.h>
#include <model/ModelFactory.h>
#include <model/SICSGeneralModel.h>
#include <estimation/classical/EMEstimation.h>
#include <input/Input.h>
#include <util/NCM.h>
#include "util/blasInterface.h"
#include <../test/EMTest.h>


using namespace std;



#endif /* SICS_H_ */
