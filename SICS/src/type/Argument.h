/*
 * Argument.h
 *
 *  Created on: 18 Jun 2014
 *      Author: jlgpisa
 */

#ifndef ARGUMENT_H_
#define ARGUMENT_H_

class Argument {
public:
	Argument();
	virtual ~Argument();
};

#endif /* ARGUMENT_H_ */
