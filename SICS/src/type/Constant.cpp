/*
 * Constant.cpp
 *
 *  Created on: 21 Jul 2014
 *      Author: jlgpisa
 */

#include <type/Constant.h>

double Constant::NORM_CONST = 1;
double Constant::MAX_EXP = 35.0;
double Constant::INFINITE = 1e30;
double Constant::EPSILON = 1e-20;
string Constant::INITIAL_VALUE_METHOD = "ANDRADE";
double Constant::stepredn =	0.2;
double Constant::acctol	=	0.0001;
double Constant::reltest	=	10.0;
double Constant::abstol  =    0.00001;
double Constant::reltol  =    1e-8;
