/*
 * TestConstant.cpp
 *
 *  Created on: 29 Jul 2014
 *      Author: jlgpisa
 */

#include "TestConstant.h"

string TestConstant::kNewConf = "NEW";
string TestConstant::kFileType = "FILETYPE";
string TestConstant::kFullPath = "FULLPATH";
string TestConstant::kHeader = "HEADER";
string TestConstant::kSep = "SEP";
string TestConstant:: kNodeCount="NODES";

string TestConstant::kDataset = "DATASET";
string TestConstant::kInitialValue = "INITIAL_VALUE";
string TestConstant::kConvergence = "CONVERGENCE";
string TestConstant::kPob = "POBLATIONAL";

int TestConstant::q = 41;
